This is a skeleton Spring 3 app. Buildable by Ant.

# to clean
ant clean

# to build war file
ant war

# source tree
src/
# lib
war/WEB-INF/lib
# JSP
war/WEB-INF/pages


Tasks:

  * Add javadoc to current source codes. 

  * Add a one-page documentation on current functionality of this webapp.
    ** What URL resources are available. If you access those URL's what are the expected output.

  * Add a new Controller to simulate a calculator to perform 
    ** add, substract, multiplication, division
    ** for two numbers

    ** show JSON request and response payload

    ** show how you test it using either wget/curl (or another tool of your choice)

  * Add a new Controller to provide a new REST service
    ** For a given IP address
    ** show its ZIP code, ISO code (2 chars), ISO code (3 chars) 
    ** Tips: you can use the following external services

# Free Restful web service to get location geolocation by IP address
http://www.groupkt.com/post/5926d648/free-restful-web-service-to-get-location-by-ip-address.htm

# To get 3-letter code
http://www.groupkt.com/post/c9b0ccb9/country-and-other-related-rest-webservices.htm

  * Bonus - show you can create an Eclipse project for this webapp: build, deploy to a server (your choice)
